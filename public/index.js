import {
    register as service_register,
    login as service_login,
    set_heatmap as service_set_heatmap
} from './service.js';

let all_heatmaps = [];
let heatmap_idx = 0;
let vw = document.documentElement.clientWidth;
let svg_width = vw;
let begin_height = vw * 0.05;
let begin_width = svg_width * 0.05;
let total_cells_width = svg_width - begin_width;
let cell_size = total_cells_width / 12;
let selected_cell = null;

const date = new Date();
const day_of_the_week = date.getDay();
const day = date.getDate();
const local_storage = window.localStorage;
const body = document.getElementById("habit_tracker");
const title = document.getElementById("title");
const title_edit_html = " <button onclick='_on_title_edit_clicked()'>rename</button>";
const clear_habit_html = " <button onclick='_on_clear_habit_clicked()'>clear</button>";

let current_heatmap = "heatmap";

function json_parse(json)
{
    try
    {
        return JSON.parse(json);
    }
    catch (error)
    {
        return false;
    }
}

function draw_v2(heatmap)
{
    vw = document.documentElement.clientWidth;
    svg_width = vw;
    begin_height = vw * 0.07;
    begin_width = svg_width * 0.05;
    total_cells_width = svg_width - begin_width;
    cell_size = total_cells_width / 12;

    body.innerHTML = "";

    if(heatmap == {} || heatmap.name == null)
    {
        let current_title = "1";

        if(current_heatmap !== "heatmap")
        {
            current_title = current_heatmap.charAt(current_heatmap.length - 1);
        }

        let new_heatmap = {map: {}, name: current_title};
        heatmap = new_heatmap;
    }

    let title_html = heatmap.name;

    if(!state_visitor)
    {
        title_html += title_edit_html + clear_habit_html;
    }

    title.innerHTML = title_html;

    const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ];

    let table = "<svg id='svg' width='" + svg_width + "' height='" + cell_size * 4 * 12 * 1.485 + "'>";

    table += "<text x='" + (1 * cell_size * 1.4 + begin_width * 1.24) + "' y='" + (begin_height - 10) + "' class='weekday'>Mon</text>";
    table += "<text x='" + (3 * cell_size * 1.4 + begin_width * 1.24) + "' y='" + (begin_height - 10) + "' class='weekday'>Wed</text>";
    table += "<text x='" + (5 * cell_size * 1.4 + begin_width * 1.24) + "' y='" + (begin_height - 10) + "' class='weekday'>Fri</text>";

    for (let i = 0; i < 7; i++)
    {
        let j = 0;
        let d = new Date();

        if (i > day_of_the_week)
        {
            j = 1;
            let diff = day_of_the_week + (7 - i);
            d.setDate(d.getDate() - diff);
        }
        else
        {
            let diff = day_of_the_week - i;
            d.setDate(d.getDate() - diff);
        }

        const x = i * cell_size * 1.4 + begin_width;
        for (; j < 4 * 12; j++)
        {
            const id = (d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear());

            table += "<rect id='" + id + "' ";

            if(!state_visitor)
            {
                table += "onclick='_on_cell_clicked(this)' ";
            }

            table += "width='" + cell_size + "' height='" + cell_size + "' y=" + (j * cell_size * 1.4 + begin_height) + " x=" + x + " onmousemove='_on_mouse_over(this);' onmouseout='_on_mouse_out();'></rect>";

            d.setDate(d.getDate() - 7);
        }

        table += "</g>";
    }

    table += "</svg>";

    body.innerHTML += table;

    const today = document.getElementById(day + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());
    today.classList.add('today');

    let m = date.getMonth() + 1;
    let y = date.getFullYear();
    let months_text = "";

    for (let j = 11; j > 0; j--)
    {
        const cell = document.getElementById("1/" + m + "/" + y);

        if(!cell)
        {
            break;
        }

        months_text += "<text x='" + (8 * cell_size * 1.25 + begin_width) + "' y='" + (Number(cell.getAttribute('y')) + begin_height) + "' class='weekday'>" + months[m - 1] + "</text>";

        if (m == 1)
        {
            m = 12;
            y--;
        }
        else
        {
            m--;
        }
    }

    document.getElementById('svg').innerHTML += months_text;

    let heatmap_data = heatmap.map;

    let max_heat = 0;
    let min_heat = 0;

    for (let id in heatmap_data)
    {
        if (max_heat < heatmap_data[id])
        {
            max_heat = heatmap_data[id];
        }

        if (min_heat > heatmap_data[id])
        {
            min_heat = heatmap_data[id];
        }
    }

    const shades_of_good = [
        "#9be9a8", // light green
        "#40c463", // greener
        "#30a14e", // greener still
        "#216e39" // greenest
    ];

    for (let id in heatmap_data)
    {
        const cell = document.getElementById(id);
        const heat = heatmap_data[id];

        if(heat <= 0)
        {
            continue;
        }

        const ratio = heat / max_heat;
        const ratio_hex_scaled = Math.ceil(ratio * (shades_of_good.length - 1));
        cell.style.fill = shades_of_good[ratio_hex_scaled];
    }

    const shades_of_bad = [
        "#575757",
        "#3c3c3c",
        "#222222",
        "#070707",
    ];

    for (let id in heatmap_data)
    {
        const cell = document.getElementById(id);
        const heat = heatmap_data[id];

        if(heat >= 0)
        {
            continue;
        }

        const ratio = Math.abs(heat) / Math.abs(min_heat);
        const ratio_hex_scaled = Math.ceil(ratio * (shades_of_bad.length - 1));
        cell.style.fill = shades_of_bad[ratio_hex_scaled];
    }
}

function draw()
{
    let heatmap = json_parse(local_storage.getItem(current_heatmap)) || {};
    
    if(heatmap == {} || heatmap.name == null)
    {        
        let current_title = "1";
        let new_heatmap = {map: {}, name: current_title};
        local_storage.setItem(current_heatmap, JSON.stringify(new_heatmap));
        heatmap = new_heatmap;
    }

    draw_v2(heatmap);
}

function update_tooltip_v2(el, heatmap)
{
    let svg = document.getElementById("svg");
    let svg_pos = get_el_position(svg);
    let tooltip = document.getElementById("tooltip");
    tooltip.innerHTML = el.id + " " + (heatmap.map[el.id] || 0);
    tooltip.style.display = "block";
    tooltip.style.left = el.getAttribute('x') + 'px';
    let cell_y = Number(el.getAttribute('y'));
    tooltip.style.top = ((svg_pos.y) + cell_y - cell_size) + 'px';
}

function update_tooltip(el)
{
    let heatmap = all_heatmaps[heatmap_idx] || {};

    if(!state_visitor)
    {
        heatmap = json_parse(local_storage.getItem(current_heatmap)) || {};
    }
    
    update_tooltip_v2(el, heatmap);
}

function _on_sync_success()
{

}

function _on_sync_failure()
{
    
}

function _on_cell_clicked(el)
{
    document.getElementById('panel').style.display = "flex";

    const heatmap = json_parse(local_storage.getItem(current_heatmap)) || {};

    document.getElementById('cell_count').innerHTML = heatmap.map[el.id] || 0;

    selected_cell = el;

    stop_event_propagation();
}

function _on_mouse_over(el)
{
    update_tooltip(el);
}

function _on_mouse_out()
{
    let tooltip = document.getElementById("tooltip");
    tooltip.style.display = "none";
}

function _on_habit_clicked(num)
{
    heatmap_idx = num - 1;

    if(state_visitor)
    {
        update_title(all_heatmaps[heatmap_idx].name || num);
        draw_v2(all_heatmaps[heatmap_idx]);
        return;
    }

    // to keep backward compatibility with users who has
    // already started to use the website when we only had
    // one heatmap without a number suffix
    if (num == 1)
    {
        current_heatmap = "heatmap";
    }
    else
    {
        current_heatmap = "heatmap" + num;
    }

    const heatmap = get_current_heatmap();

    update_title(heatmap.name || num);

    draw();
}

// Helper function to get an element's exact position
function get_el_position(el)
{
    let rect = el.getBoundingClientRect();
    return {x:rect.left + document.documentElement.scrollTop,y:rect.top + document.documentElement.scrollTop}
}

function update_title(title)
{
    const title_div = document.getElementById("title_div");

    let title_html = `
    <h3 id="title">` + title;

    if(!state_visitor)
    {
        title_html += title_edit_html + clear_habit_html;
    }

    title_html += `</h1>
    `;

    title_div.innerHTML = title_html;
}

function sync_heatmap(heatmap)
{
    const id = local_storage.getItem('id');

    if(id == null || id == "")
    {
        return;
    }

    service_set_heatmap(Number(id), current_heatmap, heatmap, _on_sync_success, _on_sync_failure);
}

function _on_habit_title_editted(new_title)
{
    update_title(new_title);
    const heatmap = get_current_heatmap();
    heatmap.name = new_title;
    sync_heatmap(heatmap);
    set_current_heatmap(heatmap);
    update_heatmap_buttons();
}

function _on_clear_habit_clicked()
{
    const current_title = get_current_heatmap().name;

    if (confirm(`You're about to CLEAR all of your progress in ` + current_title + `
        all of your progress will be LOST. Continue?`))
    {
        let h = get_current_heatmap();
        h = {};
        set_current_heatmap(h);
        sync_heatmap(h);
        update_heatmap_buttons();
        
        const num = Number(current_heatmap[current_heatmap.length - 1]);

        if(isNaN(num))
        {
            update_title(1);
        }
        else
        {
            update_title(num);
        }

        draw();
    }
}

function _on_title_edit_clicked()
{
    let current_title_s = get_current_heatmap().name || "";
    const title_div = document.getElementById("title_div");

    title_div.innerHTML =
    `<div id='title_editor'>
        <input onfocusout="_on_habit_title_editted(this.value)" id='title_input' value='`+current_title_s+`'>
    </div>`;

    const title_input = document.getElementById('title_input');
    title_input.focus();
}

function get_current_heatmap()
{
    return json_parse(local_storage.getItem(current_heatmap)) || {};
}

function set_current_heatmap(heatmap)
{
    local_storage.setItem(current_heatmap, JSON.stringify(heatmap));
}

function update_heatmap_buttons_v2(heatmaps)
{
    for(let i = 0; i < heatmaps.length; i++)
    {
        const heatmap = heatmaps[i] || {};
        const btn = document.getElementById("btn" + (i+1));
        if(heatmap.name)
        {
            btn.innerHTML = heatmap.name;
        }
        else
        {
            btn.innerHTML = (i+1);
        }
    }
}

function update_heatmap_buttons()
{
    for(let i = 1; i <= 5; i++)
    {
        let heatmap_ = "heatmap";
    
        if(i != 1)
        {
            heatmap_ = "heatmap" + i;
        }

        const heatmap = json_parse(local_storage.getItem(heatmap_)) || {};
        
        const btn = document.getElementById("btn" + i);
        if(heatmap.name)
        {
            btn.innerHTML = heatmap.name;
        }
        else
        {
            btn.innerHTML = i;
        }
    }
}

// a login that happens automatically. Happens because
// you're already registered but you're just refreshing the page
function _on_automatic_login_success(response)
{
    local_storage.setItem('heatmap', response.heatmap);
    local_storage.setItem('heatmap2', response.heatmap2);
    local_storage.setItem('heatmap3', response.heatmap3);
    local_storage.setItem('heatmap4', response.heatmap4);
    local_storage.setItem('heatmap5', response.heatmap5);

    update_heatmap_buttons();
    draw();
    state_logged_in();
}

function _on_visitor_login_success(response)
{
    all_heatmaps = [
        json_parse(response.heatmap),
        json_parse(response.heatmap2),
        json_parse(response.heatmap3),
        json_parse(response.heatmap4),
        json_parse(response.heatmap5),
    ];

    update_heatmap_buttons_v2(all_heatmaps);
    draw_v2(all_heatmaps[0]);
}

// A login that happens by an active user action. Happens because
// you're already registered but you're clicked the "I already have a sync code"
// button
function _on_manual_login_success(response)
{
    local_storage.setItem('heatmap', response.heatmap);
    local_storage.setItem('heatmap2', response.heatmap2);
    local_storage.setItem('heatmap3', response.heatmap3);
    local_storage.setItem('heatmap4', response.heatmap4);
    local_storage.setItem('heatmap5', response.heatmap5);
    local_storage.setItem('id', document.getElementById('entered_sync_code').value);

    update_heatmap_buttons();
    draw();
    state_sync_code_shown();
}

function _on_login_failure(status, response)
{
    state_unregistered();
    const sync_error = document.getElementById('sync_error');

    let error = 'The sync code is incorrect.';

    if(status >= 500)
    {
        error = 'response code: ' + status + '</br>' +
                'response error: ' + response +
                '</br>Please send this error my way so that I can fix it adhamzahranfms@gmail.com';
    }

    sync_error.innerHTML = error;
}

function _on_registration_success(response)
{
    local_storage.setItem('id', response.id);
    state_sync_code_shown();
}

function _on_registration_failure(status, error)
{
    sync_error = document.getElementById('sync_error');
    sync_error.innerHTML = 'response code: ' + status + '</br>' +
                          'response error: ' + error + '</br>' +
                          'Please send this error my way so that I can fix it adhamzahranfms@gmail.com';
}

function _on_create_sync_code_btn_clicked()
{
    service_register(
        local_storage.getItem('heatmap'),
        local_storage.getItem('heatmap2'),
        local_storage.getItem('heatmap3'),
        local_storage.getItem('heatmap4'),
        local_storage.getItem('heatmap5'),
        _on_registration_success,
        _on_registration_failure
    );
}

function state_unregistered()
{
    const sync_stuff = document.getElementById('sync_stuff');
    sync_stuff.innerHTML =
    `<p style='color:#444'>Sync with other devices and avoid losing your data</p>
    <button id='enter_sync_code' onclick='_on_enter_sync_code_btn_clicked()'>I Already Have a Sync Code</button>
    <button id='create_sync_code' onclick='_on_create_sync_code_btn_clicked()'>Create a New Sync Code</button>`;

    document.getElementById('sync_code').innerHTML = "";
    document.getElementById('sync_error').innerHTML = "";
}

function state_logged_in()
{
    const sync_stuff = document.getElementById('sync_stuff');

    sync_stuff.innerHTML = "<button id='show_sync_code' onclick='_on_show_sync_code_btn_clicked()'>Show My Sync Code</button> <button id='share_btn' onclick='_on_share_btn_clicked()'>Share</button>";
    document.getElementById('sync_code').innerHTML = "";
    document.getElementById('sync_error').innerHTML = "";
}

function state_sync_code_shown()
{
    document.getElementById('sync_code').innerHTML = local_storage.getItem('id');
    document.getElementById('sync_error').innerHTML = "Treat this like a password. Save it in your note taking app or something";
    
    const sync_stuff = document.getElementById('sync_stuff');
    sync_stuff.innerHTML =
    `<button onclick='_on_hide_sync_code_btn_clicked()'>Hide My Sync Code</button>`;
}

function _on_hide_sync_code_btn_clicked()
{
    state_logged_in();
}

function _on_show_sync_code_btn_clicked()
{
    state_sync_code_shown();
}

function _on_share_btn_clicked()
{
    let get_url = window.location;
    let base_url = get_url.protocol + "//" + get_url.host;
    document.getElementById('sync_code').innerHTML = base_url + "?sync_code=" + local_storage.getItem('id');
    document.getElementById('sync_error').innerHTML = "Treat this like a password. Only give it to someone you trust";
    
    const sync_stuff = document.getElementById('sync_stuff');
    sync_stuff.innerHTML =
    `<button onclick='_on_hide_sync_code_btn_clicked()'>Hide My Share Link</button>`;
}

function _on_submit_sync_code_btn_clicked()
{
    if (confirm(`You will sync using the code you entered but
                     YOU WILL LOSE ALL OF YOUR PROGRESS (if any).
                     If you click cancel, you will NOT sync, and your progress will not be lost`))
    {
        service_login(document.getElementById('entered_sync_code').value, _on_manual_login_success, _on_login_failure);
    }
    else
    {
        state_unregistered();
    }
}

function _on_enter_sync_code_btn_clicked()
{
    const sync_stuff = document.getElementById('sync_stuff');
    sync_stuff.innerHTML =
    `<input id='entered_sync_code' placeholder='Enter your sync code here'></input>
    <button onclick='_on_submit_sync_code_btn_clicked()'>submit</button>`;
}

function _on_window_resize()
{
    if(state_visitor)
    {
        draw_v2(all_heatmaps[heatmap_idx]);
    }
    else
    {
        draw();
    }
}

const query_string = window.location.search;
const url_params = new URLSearchParams(query_string);
const url_sync_code = url_params.get('sync_code') || undefined;
let state_visitor = false;

if(url_sync_code !== undefined)
{
    service_login(url_sync_code, _on_visitor_login_success, _on_login_failure);
    state_visitor = true;
}

if(!state_visitor)
{
    const id = local_storage.getItem('id');

    if(id != null && id != "")
    {
        service_login(id, _on_automatic_login_success, _on_login_failure);
    }
    else
    {
        state_unregistered();
    }
}

function _on_panel_minus_clicked()
{
    const heatmap = json_parse(local_storage.getItem(current_heatmap)) || {};

    let el = selected_cell;

    if (heatmap.map[el.id] === undefined)
    {
        heatmap.map[el.id] = -1;
    }
    else
    {
        heatmap.map[el.id]--;
    }

    local_storage.setItem(current_heatmap, JSON.stringify(heatmap));
    sync_heatmap(heatmap);

    document.getElementById('cell_count').innerHTML = heatmap.map[el.id];
    update_tooltip(el);
    draw();
    stop_event_propagation();
}

function _on_panel_plus_clicked()
{
    const heatmap = json_parse(local_storage.getItem(current_heatmap)) || {};

    let el = selected_cell;

    if (heatmap.map[el.id] === undefined)
    {
        heatmap.map[el.id] = 1;
    }
    else
    {
        heatmap.map[el.id]++;
    }

    local_storage.setItem(current_heatmap, JSON.stringify(heatmap));
    sync_heatmap(heatmap);

    document.getElementById('cell_count').innerHTML = heatmap.map[el.id];
    update_tooltip(el);
    draw();
    stop_event_propagation();
}

function _on_clicked_nowhere()
{
    console.log('_on_clicked_nowhere');
    selected_cell = null;
    document.getElementById('panel').style.display = 'none';
}

function stop_event_propagation(e)
{
    //stop the event propagating to the body element
    let evt = e ? e : window.event;

    if (evt.stopPropagation) {evt.stopPropagation();}
    else {evt.cancelBubble=true;}
    return false;
}

function _on_panel_clicked()
{
    stop_event_propagation();
}

if(!state_visitor)
{
    window._on_submit_sync_code_btn_clicked = _on_submit_sync_code_btn_clicked;
    window._on_create_sync_code_btn_clicked = _on_create_sync_code_btn_clicked;
    window._on_enter_sync_code_btn_clicked  = _on_enter_sync_code_btn_clicked;
    window._on_hide_sync_code_btn_clicked   = _on_hide_sync_code_btn_clicked;
    window._on_show_sync_code_btn_clicked   = _on_show_sync_code_btn_clicked;
    window._on_panel_minus_clicked          = _on_panel_minus_clicked;
    window._on_habit_title_editted          = _on_habit_title_editted;
    window._on_clear_habit_clicked          = _on_clear_habit_clicked;
    window._on_panel_plus_clicked           = _on_panel_plus_clicked;
    window._on_title_edit_clicked           = _on_title_edit_clicked;
    window._on_share_btn_clicked            = _on_share_btn_clicked;
    window._on_panel_clicked                = _on_panel_clicked;
    window._on_cell_clicked                 = _on_cell_clicked;

    document.getElementsByTagName('body')[0].onclick = _on_clicked_nowhere;
}

window._on_habit_clicked = _on_habit_clicked;
window._on_mouse_over    = _on_mouse_over;
window._on_mouse_out     = _on_mouse_out;

window.addEventListener("resize", _on_window_resize);

update_heatmap_buttons();
draw();