export {register, login, set_heatmap};

function register(heatmap, heatmap2, heatmap3, heatmap4, heatmap5, onsuccess, onfailure)
{
    const data = {};

    if(heatmap != null && heatmap != "")
    {
        data['heatmap'] = heatmap;
    }

    if(heatmap2 != null && heatmap2 != "")
    {
        data['heatmap2'] = heatmap2;
    }

    if(heatmap3 != null && heatmap3 != "")
    {
        data['heatmap3'] = heatmap3;
    }

    if(heatmap4 != null && heatmap4 != "")
    {
        data['heatmap4'] = heatmap4;
    }

    if(heatmap5 != null && heatmap5 != "")
    {
        data['heatmap5'] = heatmap5;
    }

    http_post('api/register', JSON.stringify(data),
        (response) => {
            //log_success(response);
            onsuccess(JSON.parse(response));
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });
}

function login(id, onsuccess, onfailure)
{
    const data = {id: Number(id)};
    http_post('api/login', JSON.stringify(data),
        (response) => {
            //log_success(response);
            onsuccess(JSON.parse(response));
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });
}

function set_heatmap(id, heatmap_name, heatmap_value, onsuccess, onfailure)
{
    var data = {};
    data[heatmap_name] = heatmap_value;
    data['id'] = Number(id);

    http_post('api/set_heatmap', JSON.stringify(data),
        (response) => {
            log_success(response);
            onsuccess();
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });
}

function http_request(url, method, data, onsuccess, onfailure)
{
    console.log("sending request");
    var http = new XMLHttpRequest();
    http.open(method, url, true); // true for asynchronous
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(data);

    http.onreadystatechange = function()
    {
        if (http.readyState == 4 && http.status == 200)
        {
            onsuccess(http.responseText);
            return;
        }
        else if(http.readyState == 4 && http.status != 200)
        {
            onfailure(http.status, http.responseText);
        }
    }
}

function http_post(url, data, onsuccess, onfailure)
{
    http_request(url, "POST", data, onsuccess, onfailure);
}

function log_success(response)
{
    console.log("response received");
    console.log("response: " + response);
    console.log("Server responded with 200 success");
}

function log_failure(status, response)
{
    console.log("Request failed, server responded with: ");
    console.log(response);
    console.log("response code: " + status);
}