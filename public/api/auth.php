<?php

function auth($id)
{
    if(!is_numeric($id))
    {
        http_response_code(401);
        echo 'bad id: Not a number';
        die();
    }

    $id = (int)$id;

    $sqlite = sqlite_open(sqlite_db_path());

    $query = sprintf(
    'SELECT * FROM users WHERE id=%d',
    $id);

    $result = sqlite_query($sqlite, $query, "Could not login");

    if($result == null || empty($result))
    {
        http_response_code(401);
        echo 'bad id: Does not exist';
        die();
    }

    if(count($result) > 1)
    {
        http_response_code(500);
        echo 'more than 1 user has the same id... ops';
        die();
    }

    return $result[0];
}