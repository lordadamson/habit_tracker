<?php

require_once 'sqlite.php';
require_once 'config.php';

$sqlite = sqlite_open(sqlite_db_path());

sqlite_prep_n_exec($sqlite, 
'create table users (
    id INTEGER NOT NULL UNIQUE,
    heatmap  TEXT,
    heatmap2 TEXT,
    heatmap3 TEXT,
    heatmap4 TEXT,
    heatmap5 TEXT
);',
// error to display in case of error:
"Couldn't create the users table"
);

echo "All Good!
\033[01;31m
don't forget to delete install.php\n\033[0m";