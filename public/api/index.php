<?php

require_once 'set_heatmap.php';
require_once 'login.php';
require_once 'register.php';
require_once 'utils.php';
require_once 'router.php';

$request_size = (int) $_SERVER['CONTENT_LENGTH'];

// 5 MB
if($request_size > 5242880)
{
    http_response_code(400);
    user_error_log('400 request too big');
    die();
}

post('register', 'register');
post('login', 'login');
post('set_heatmap', 'set_heatmap');

http_response_code(404);
user_error_log('404 not found');