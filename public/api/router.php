<?php

$request = $_SERVER['REQUEST_URI'];
$request = substr($request, 5); // removing the '/api/' part
$method  = $_SERVER['REQUEST_METHOD'];

if(substr($request, -1) == '/')
{
    string_pop($request);
}

function http_request(string $_method, string $uri, $callback)
{
    global $request;
    global $method;

    if($request != $uri)
    {
        return;
    }

    if($method != $_method)
    {
        return;
    }

    $callback();

    die();
}

function post(string $uri, $callback)
{
    http_request('POST', $uri, $callback);
}

function get(string $uri, $callback)
{
    http_request('GET', $uri, $callback);
}