<?php

function sqlite_open(string $path)
{
    $error_msg = "Couldn't create the db connection";

    if($path == "" || $path == null)
    {
        _sqlite_error($error_msg);
    }

    try
    {
        $sqlite = new PDO('sqlite:' . $path);
        $sqlite->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    }
    catch(PDOException $e)
    {
        _sqlite_error($error_msg .' (open): '. $e->getMessage());
    }

    return $sqlite;
}

function sqlite_prep_n_exec($sqlite, string $query, string $error_msg)
{
    $statement = sqlite_prepare($sqlite, $query, $error_msg);
    sqlite_execute_statement($statement, [], $error_msg);
}

function sqlite_prepare($sqlite, string $query, string $error_msg)
{
    $result = $sqlite->prepare($query);

    if($result == false)
    {
        _sqlite_error($error_msg .' (prepare): '. $sqlite->errorInfo()[2]);
    }

    return $result;
}

function sqlite_execute_statement(PDOStatement $statement, array $values, string $error_msg)
{
    $result = $statement->execute($values);
    
    if($result == false)
    {
        _sqlite_error($error_msg .' (execute): '. $statement->errorInfo()[2]);
    }

    return $result;
}

function sqlite_query($sqlite, string $query, string $error_msg)
{
    $result = $sqlite->query($query);

    if($result == false)
    {
        _sqlite_error($error_msg .' (query): '. $sqlite->errorInfo()[2]);
    }

    return $result->fetchAll();
}

function _sqlite_error($error)
{
    http_response_code(500);
    echo $error . "</br>\n";
    die();
}