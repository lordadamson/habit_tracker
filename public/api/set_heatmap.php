<?php

require_once 'auth.php';
require_once 'sqlite.php';
require_once 'config.php';

function set_heatmap()
{
    $json = json_decode_from_input();
    $id = $json->id;
    auth($json->id);

    $sqlite = sqlite_open(sqlite_db_path());

    if(isset($json->heatmap))
    {
        $heatmap_name = 'heatmap';
        $heatmap_value = $json->heatmap;
    }
    else if(isset($json->heatmap2))
    {
        $heatmap_name = 'heatmap2';
        $heatmap_value = $json->heatmap2;
    }
    else if(isset($json->heatmap3))
    {
        $heatmap_name = 'heatmap3';
        $heatmap_value = $json->heatmap3;
    }
    else if(isset($json->heatmap4))
    {
        $heatmap_name = 'heatmap4';
        $heatmap_value = $json->heatmap4;
    }
    else if(isset($json->heatmap5))
    {
        $heatmap_name = 'heatmap5';
        $heatmap_value = $json->heatmap5;
    }
    else
    {
        http_response_code(400);
        echo "bad heatmap name";
        die();
    }

    if(!isset($heatmap_name))
    {
        http_response_code(400);
        echo "where's the heatmap at bro?";
        die();
    }

    _validate_heatmap($heatmap_value);

    $heatmap_value = json_encode($heatmap_value);

    $statement = sqlite_prepare($sqlite,
    'UPDATE users
    SET
        '.$heatmap_name.' = :heatmap_value
    WHERE
        id = :id',
    /*Error message in case of failure*/
    "Could not update the heatmap");


    sqlite_execute_statement($statement, array(
       'heatmap_value' => $heatmap_value,
       'id' => $id
    ),
    /*Error message in case of failure*/
    "Could not update the heatmap");

    echo 'success';
}

function _validate_heatmap($heatmap)
{
    // TODO
}