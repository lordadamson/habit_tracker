<?php

require_once 'sqlite.php';
require_once 'config.php';

function register()
{
    $sqlite = sqlite_open(sqlite_db_path());

    // Any higher and we'll hit unpredictable behavior on
    // php, sqlite, js, or whatever other client is gonna receive
    // this value
    $id = random_int(0, 9000000000000000);

    $json = file_get_contents('php://input');
    $json = json_decode($json);

    $heatmap  = "{}";
    $heatmap2 = "{}";
    $heatmap3 = "{}";
    $heatmap4 = "{}";
    $heatmap5 = "{}";

    if(isset($json->heatmap))
    {
        $heatmap = $json->heatmap;
    }
    if(isset($json->heatmap2))
    {
        $heatmap2 = $json->heatmap2;
    }
    if(isset($json->heatmap3))
    {
        $heatmap3 = $json->heatmap3;
    }
    if(isset($json->heatmap4))
    {
        $heatmap4 = $json->heatmap4;
    }
    if(isset($json->heatmap5))
    {
        $heatmap5 = $json->heatmap5;
    }

    $error_msg = "Could not register a new user";

    $statement = sqlite_prepare($sqlite,
    'INSERT INTO users (id, heatmap, heatmap2, heatmap3, heatmap4, heatmap5)
    VALUES (:id, :heatmap, :heatmap2, :heatmap3, :heatmap4, :heatmap5)',
    /*Error message in case of failure*/
    $error_msg);

    sqlite_execute_statement($statement, array(
        'id' => $id,
        'heatmap' => $heatmap,
        'heatmap2' => $heatmap2,
        'heatmap3' => $heatmap3,
        'heatmap4' => $heatmap4,
        'heatmap5' => $heatmap5
    ),
    /*Error message in case of failure*/
    $error_msg);

    $response = new stdClass();
    $response->id = $id;

    echo json_encode($response);
}