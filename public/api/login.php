<?php

require_once 'auth.php';
require_once 'utils.php';
require_once 'sqlite.php';
require_once 'config.php';

function login()
{
    $json = json_decode_from_input();

    $user = auth($json->id);

    $response = new stdClass();
    $response->heatmap  = $user['heatmap'];
    $response->heatmap2 = $user['heatmap2'];
    $response->heatmap3 = $user['heatmap3'];
    $response->heatmap4 = $user['heatmap4'];
    $response->heatmap5 = $user['heatmap5'];

    echo json_encode($response);
}